package br.com.listo.receivables.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;


public class Receivable {
  private UUID id;
  private BigDecimal amount;
  private Integer installmentQty;
  private BigDecimal lossAmount;
  private LocalDateTime createdDate;


  public Receivable(UUID id,
                    BigDecimal amount,
                    LocalDateTime createdDate) {
    this.id = id;
    this.createdDate = createdDate;
    setAmount(amount);
  }


  public BigDecimal getAmount() {
    return amount;
  }


  private void setAmount(BigDecimal amount) {
    this.amount = amount;
  }


  public Integer getInstallmentQty() {
    return installmentQty;
  }


  public void setInstallmentQty(int installmentQty) {
    this.installmentQty = installmentQty;
  }


  public BigDecimal getLossAmount() {
    return lossAmount;
  }


  public void setLossAmount(BigDecimal lossAmount) {
    this.lossAmount = lossAmount;
  }


  public LocalDateTime getCreatedDate() {
    return createdDate;
  }


  public UUID getId() {
    return id;
  }
}
