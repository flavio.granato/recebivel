package br.com.listo.receivables.domain;

import br.com.listo.receivables.infra.InfraContainer;


public class DomainContainer {
  public static DataGeneratorService getDataGeneratorService() {
    return new DataGeneratorService(InfraContainer.getDataGenerator());
  }
}
