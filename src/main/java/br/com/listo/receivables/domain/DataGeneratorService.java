package br.com.listo.receivables.domain;

import br.com.listo.receivables.entity.Receivable;
import br.com.listo.receivables.infra.DataGenerator;
import com.aol.cyclops.control.StreamUtils;
import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class DataGeneratorService {
  private DataGenerator dataGenerator;


  public DataGeneratorService(DataGenerator dataGenerator) {
    this.dataGenerator = dataGenerator;
  }


  public List<Receivable> generate() {
    int numberOfParcels = 12;
    List<Receivable> receivables = new ArrayList<>();

    receivables.addAll(dataGenerator.generate(1000,
                                              50,
                                              700));
    receivables.addAll(dataGenerator.generate(8000,
                                              701,
                                              2000));
    receivables.addAll(dataGenerator.generate(1000,
                                              2001,
                                              10000));

    Collections.shuffle(receivables);
    List<Receivable> receivablesWithInstallments = generateInstallments(numberOfParcels,
                                                                        receivables);
    List<Receivable> receivablesWithLossesAmount = generateLossAmount(receivablesWithInstallments);

    return receivablesWithLossesAmount;
  }


  private List<Receivable> generateLossAmount(List<Receivable> receivablesWithInstallments) {
    List<Receivable> list = new ArrayList<>();

    for (Receivable receivable : receivablesWithInstallments) {
      if (1 == receivable.getInstallmentQty()) {
        Double loss = 0.10d; // 0,10%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      } else if (2 == receivable.getInstallmentQty()) {
        Double loss = 0.08d; // 0,08%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      } else if (3 == receivable.getInstallmentQty()) {
        Double loss = 0.06d; // 0,06%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      } else if (4 == receivable.getInstallmentQty()) {
        Double loss = 0.04d; // 0,04%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      } else if (5 == receivable.getInstallmentQty()) {
        Double loss = 0.03d; // 0,03%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      } else if (6 == receivable.getInstallmentQty()) {
        Double loss = 0.02d; // 0,02%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      } else if (receivable.getInstallmentQty() >= 7 || receivable.getInstallmentQty() <= 12) {
        Double loss = 0.01d; // 0,01%
        receivable.setLossAmount(dataGenerator.calculateLoss(receivable.getAmount(),
                                                             loss));
      }

      list.add(receivable);
    }

    return list;
  }


  private List<Receivable> generateInstallments(int numberOfParcels,
                                                List<Receivable> receivables) {
    int partitionSize = receivables.size() / numberOfParcels;
    int installmentNumber = 1;

    for (List<Receivable> partition : Lists.partition(receivables,
                                                      partitionSize)) {
      if (partition.size() < partitionSize) {
        for (Receivable r : partition) {
          r.setInstallmentQty(installmentNumber - 1);
        }
      } else {
        for (Receivable r : partition) {
          r.setInstallmentQty(installmentNumber);
        }
        installmentNumber = installmentNumber + 1;
      }
    }

    return receivables;
  }


  public List<Receivable> decreaseLosses(List<Receivable> receivables,
                                         BigDecimal amount) {
    receivables.sort(Comparator.comparing(Receivable::getLossAmount));

    List<Receivable> receivablesSortedByLosses = StreamUtils.reversedStream(receivables)
                                                            .collect(Collectors.toList());

    return findBettersLossesToTransfer(amount,
                                       receivablesSortedByLosses);
  }


  private List<Receivable> findBettersLossesToTransfer(BigDecimal amount,
                                                       List<Receivable> receivablesSortedByLosses) {
    List<Receivable> result = new ArrayList<>();

    for (Receivable receivable : receivablesSortedByLosses) {
      BigDecimal sumAmount = sumAmount(result);

      if (sumAmount.compareTo(amount) == -1) {
        result.add(receivable);
      } else if (sumAmount.compareTo(amount) == 1) {
        result.remove(result.size() - 4);
      } else {
        break;
      }
    }
    return result;
  }


  private BigDecimal sumAmount(List<Receivable> receivables) {
    return receivables.stream()
                      .map(Receivable::getAmount)
                      .reduce(BigDecimal.ZERO,
                              BigDecimal::add);
  }
}
