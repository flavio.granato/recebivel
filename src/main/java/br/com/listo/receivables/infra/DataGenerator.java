package br.com.listo.receivables.infra;


import br.com.listo.receivables.entity.Receivable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;


public class DataGenerator {
  public List<Receivable> generate(Integer length,
                                   Integer minValue,
                                   Integer maxValue) {
    List<Receivable> result = new ArrayList<>(length);
    Random r = new Random();

    for (int i = 1; i <= length; i++) {
      Integer value = r.nextInt((maxValue - minValue) + 1) + minValue;

      Receivable rec = new Receivable(UUID.randomUUID(),
                                      new BigDecimal(value),
                                      LocalDateTime.now());

      result.add(rec);
    }

    return result;
  }


  public boolean hasSomeOneOutOfRange(List<Receivable> recebiveis,
                                      BigDecimal minValue,
                                      BigDecimal maxValue) {
    return recebiveis.stream()
                     .anyMatch(r -> r.getAmount()
                                     .compareTo(minValue) == -1 &&
                                    r.getAmount()
                                     .compareTo(maxValue) == 1);
  }


  public BigDecimal calculateLoss(BigDecimal value,
                                  Double loss) {
    BigDecimal percentage = BigDecimal.valueOf(loss / 100);
    return percentage.multiply(value)
                     .setScale(2,
                               BigDecimal.ROUND_HALF_UP);
  }
}
