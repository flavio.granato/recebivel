package br.com.listo.receivables.ui;

import br.com.listo.receivables.domain.DataGeneratorService;
import br.com.listo.receivables.domain.DomainContainer;
import br.com.listo.receivables.entity.Receivable;
import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class Runner {
  public static void main(String[] args) throws Exception {
    BigDecimal expected = new BigDecimal(700_000);
    DataGeneratorService service = DomainContainer.getDataGeneratorService();
    List<Receivable> receivables = service.generate();
    List<Receivable> receivablesToTransfer = service.decreaseLosses(receivables,
                                                                    expected);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
    String filenamePrefix = LocalDateTime.now()
                                         .format(formatter);

    writeDataFile(filenamePrefix,
                  receivables);
    writeResultFile(filenamePrefix,
                    receivablesToTransfer);
  }


  private static void writeResultFile(String filenamePrefix,
                                      List<Receivable> receivablesToTransfer) throws Exception {
    String filename = String.format("/tmp/%s_result.csv",
                                    filenamePrefix);
    Path path = Paths.get(filename);
    writeFile(receivablesToTransfer,
              path);
  }


  private static void writeDataFile(String filenamePrefix,
                                    List<Receivable> receivables) throws Exception {
    String filename = String.format("/tmp/%s_data.csv",
                                    filenamePrefix);
    Path path = Paths.get(filename);
    writeFile(receivables,
              path);
  }


  private static void writeFile(List<Receivable> receivables,
                                Path path) throws Exception {
    try (BufferedWriter writer = Files.newBufferedWriter(path)) {
      writer.write("\"ID\";");
      writer.write("\"Amount\";");
      writer.write("\"InstallmentQty\";");
      writer.write("\"Loss amount\";");
      writer.write("\"Created date\"\n");

      for (Receivable receivable : receivables) {
        writer.write("\"" +
                     receivable.getId()
                               .toString() +
                     "\";");
        writer.write("\"" +
                     receivable.getAmount()
                               .toString() +
                     "\";");
        writer.write("\"" +
                     receivable.getInstallmentQty()
                               .toString() +
                     "\";");
        writer.write("\"" +
                     receivable.getLossAmount()
                               .toString() +
                     "\";");
        writer.write("\"" +
                     receivable.getCreatedDate()
                               .toString() +
                     "\"\n");
      }
    }
  }
}
