package br.com.listo.receivables.domain;

import br.com.listo.receivables.entity.Receivable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class DataGeneratorServiceTest {
  private List<Receivable> receivables;
  private DataGeneratorService service;


  @BeforeMethod
  public void setUp() throws Exception {
    service = DomainContainer.getDataGeneratorService();

    receivables = new ArrayList<>();
    receivables.addAll(service.generate());
  }


  @Test
  public void testTransferReceivables() {
    BigDecimal expected = new BigDecimal(700_000);
    BigDecimal sum;
    int percentageHit = 0;


    for (int i = 1; i <= 100; i++) {
      List<Receivable> receivablesToTransfer = service.decreaseLosses(service.generate(),
                                                                      expected);
      sum = receivablesToTransfer.stream()
                                 .map(Receivable::getAmount)
                                 .reduce(BigDecimal.ZERO,
                                         BigDecimal::add);

      if (sum.compareTo(expected) != 0) {
        percentageHit = percentageHit + 1;
      }
    }

    assertTrue(percentageHit <= 5);
  }


  @Test
  public void testAmountSum() {
    BigDecimal expected = new BigDecimal(700_000);
    BigDecimal sum = receivables.stream()
                                .map(Receivable::getAmount)
                                .reduce(BigDecimal.ZERO,
                                        BigDecimal::add);

    assertTrue(sum.compareTo(expected) == 1);
  }


  @Test
  public void testGenerateAllData() {
    int expected = 10000;

    assertEquals(receivables.size(),
                 expected);
  }


  @Test
  public void testHasTwelveGroups() {
    int expected = 12;
    Set<Integer> installments = new HashSet<>();

    for (Receivable receivable : receivables) {
      Integer installment = receivable.getInstallmentQty();

      if (installments.contains(installment)) {
        // nothing to do
      } else {
        installments.add(installment);
      }
    }

    assertEquals(installments.size(),
                 expected);
  }


  @Test
  public void testLosses() {
    for (Receivable r : receivables) {
      assertNotNull(r.getLossAmount());
    }
  }
}
