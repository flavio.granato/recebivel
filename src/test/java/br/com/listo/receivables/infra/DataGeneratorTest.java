package br.com.listo.receivables.infra;

import br.com.listo.receivables.entity.Receivable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import org.testng.annotations.Test;


public class DataGeneratorTest {
  @Test
  public void testGenerateFirstDataSlice() {
    List<Receivable> receivables = new ArrayList<>();
    DataGenerator dg = InfraContainer.getDataGenerator();

    receivables.addAll(dg.generate(1000,
                                   50,
                                   700));

    assertEquals(receivables.size(),
                 1000);
    assertFalse(dg.hasSomeOneOutOfRange(receivables,
                                        new BigDecimal(50),
                                        new BigDecimal(700)));
  }


  @Test
  public void testGenerateValueLoss() {
    BigDecimal expected = new BigDecimal("1.00");
    DataGenerator dg = InfraContainer.getDataGenerator();

    BigDecimal value = new BigDecimal("1000");
    Double loss = 0.10d; // 0.10%
    BigDecimal calculatedLoss = dg.calculateLoss(value,
                                                 loss);
    assertEquals(calculatedLoss,
                 expected);
  }
}
